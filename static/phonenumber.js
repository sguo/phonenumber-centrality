
$(function() {
  show_loader(false);
});

$('#get-phone-number').on('click', function (e) {
  e.preventDefault(); // disable the default form submit event

  show_loader(true);
  
  var phone_number = $('#phone-number')[0].value;
  if (phone_number.trim().length == 0) {
    $('.invalid-feedback').show()
    show_loader(false);
    return
  }
  else {
    $('.invalid-feedback').hide()
  }
    
  url = "http://phonenumber-centrality-env.kuk7puwzk3.ap-southeast-2.elasticbeanstalk.com/v1/phonenumber/" + phone_number
    
  $.ajax({
    url: url,
    dataType: 'json',
    success: function (data) {
      if (data.status == 'success') {
        $('#formatted-number').append('<div class="alert alert-success alert-dismissible fade show" role="alert">\
            <p>For international: <a href="tel:' + data.call + '">' + data.international + '</a></p>\
            <p>For domestic: <a href="tel:' + data.call + '">' + data.national + '</a></p>\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
              <span aria-hidden="true">&times;</span>\
            </button>\
          </div>');
      }
      else {
        $('#formatted-number').append('<div class="alert alert-danger alert-dismissible fade show" role="alert">\
            <p>Found no phone numbers.</p>\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
              <span aria-hidden="true">&times;</span>\
            </button>\
          </div>');
      }
      show_loader(false);
    }
  });
});

function show_loader(show) {
  if (show) {
    $("#storm-loader").show("slow");
    $("#storm-overlay").show();
  }
  else {
    $("#storm-loader").hide();
    $("#storm-overlay").hide();
  }
}
