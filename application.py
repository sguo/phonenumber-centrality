from __future__ import print_function
from flask import Flask, render_template
from flask import request, jsonify   

from flask_restful import Resource, Api

import phonenumbers as pn
import requests


# EB looks for an 'application' callable by default.
application = Flask(__name__)
api = Api(application)

def get_ip():
    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        ip = request.remote_addr
    return ip

class PhoneNumber(Resource):
    def get(self, number):
        is_valid = False
        try:
            phone_number = pn.parse(number, None)
            is_valid = pn.is_valid_number(phone_number)
        except:
            is_valid = False
             
        if not is_valid:
            ip = get_ip()
            location = requests.get('http://ip-api.com/json/' + ip)
            if location.json()['status'] == 'success':
                country_code = location.json()['countryCode'].encode('ascii', 'ignore')
                try:
                    phone_number = pn.parse(number, country_code)
                    is_valid = pn.is_valid_number(phone_number)
                except:
                    is_valid = False
            else:
                is_valid = False
 
        if is_valid:
            international = pn.format_number(phone_number, pn.PhoneNumberFormat.INTERNATIONAL)
            national = pn.format_number(phone_number, pn.PhoneNumberFormat.NATIONAL)
            call = pn.format_number(phone_number, pn.PhoneNumberFormat.E164)
            response = {"international": international, "national": national, "call": call, "status": "success"}
        else:
            response = {"status": "fail"}
        return response
 
class Ip(Resource):
    def get(self):
        ip = get_ip()
        return jsonify({"ip": ip})

api.add_resource(PhoneNumber, '/v1/phonenumber/<string:number>')
api.add_resource(Ip, '/v1/ip')


@application.route("/")
def index():
    return render_template('onepager.html')
    

# run the app.
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    application.debug = True
    application.run()

